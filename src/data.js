/*
 * GLOBALS
 */
const optionsArray = [
  {
    label: 'Afghanistan',
    value: 'AFG'
  },
  {
    label: 'Åland Islands',
    value: 'ALA'
  },
  {
    label: 'Albania',
    value: 'ALB'
  },
  {
    label: 'Bangladesh',
    value: 'BGD'
  },
  {
    label: 'Germany',
    value: 'DEU'
  }
]


/*
 * EXPORTS
 */
export default optionsArray
