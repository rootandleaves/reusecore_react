/*
 * IMPORTS
 */
import React from 'react' // NPM: react.js library.
import ReactDOM from 'react-dom' // NPM: react-dom library.

/*
 * SIBLINGS
 */
import App from './App'
import * as serviceWorker from './serviceWorker'


/*
 * ASSETS
 */
require('./index.css')


// React dom render app component.
ReactDOM.render(<App />, document.getElementById('root'))

/*
 * If you want your app to work offline and load faster, you can change
 * unregister() to register() below. Note this comes with some pitfalls.
 */
serviceWorker.unregister()
