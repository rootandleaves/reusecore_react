/*
 * GLOBALS
 */
const pad = (num, totalChars) => {
  // Const assignment.
  const pad = '0'

  // Variable assignment.
  num = `${num}`

  // Loop over total pads.
  while (num.length < totalChars) {
    num = pad + num
  }

  // Return num.
  return num
}
const changeColor = (color, ratio, darker) => {
  // Trim trailing/leading whitespace
  color = color.replace(/^\s*|\s*$/u, '')

  // Expand three-digit hex
  color = color.replace(/^#?([a-f0-9])([a-f0-9])([a-f0-9])$/iu, '#$1$1$2$2$3$3')

  // Calculate ratio
  const difference = Math.round(ratio * 256) * (darker ? -1 : 1)
  // Determine if input is RGB(A)
  const rgb = color.match(
    // eslint-disable-next-line require-unicode-regexp
    new RegExp(
      '^rgba?\\(\\s*' + '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' + '\\s*,\\s*' + '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' + '\\s*,\\s*' + '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' + '(?:\\s*,\\s*' + '(0|1|0?\\.\\d+))?' + '\\s*\\)$',
      'i'
    )
  )
  const alpha = Boolean(rgb) && null !== rgb[4] ? rgb[4] : null
  // Convert hex to decimal
  const decimal = rgb ? [rgb[1], rgb[2], rgb[3]] : color
    .replace(
      /^#?([a-f0-9][a-f0-9])([a-f0-9][a-f0-9])([a-f0-9][a-f0-9])/iu,
      () => (
        `${parseInt(arguments[1], 16)},${parseInt(arguments[2], 16)},${parseInt(arguments[3], 16)}`
      )
    )
    .split(/,/u)

  // Return RGB(A)
  return rgb ? `rgb${
    null === alpha ? '' : 'a'
  }(${
    Math[darker ? 'max' : 'min'](
      parseInt(decimal[0], 10) + difference,
      darker ? 0 : 255
    )
  }, ${
    Math[darker ? 'max' : 'min'](
      parseInt(decimal[1], 10) + difference,
      darker ? 0 : 255
    )
  }, ${
    Math[darker ? 'max' : 'min'](
      parseInt(decimal[2], 10) + difference,
      darker ? 0 : 255
    )
  }${null !== alpha ? `, ${alpha}` : ''
  })` : [
    '#',
    pad(
      Math[darker ? 'max' : 'min'](
        parseInt(decimal[0], 10) + difference,
        darker ? 0 : 255
      ).toString(16),
      2
    ),
    pad(
      Math[darker ? 'max' : 'min'](
        parseInt(decimal[1], 10) + difference,
        darker ? 0 : 255
      ).toString(16),
      2
    ),
    pad(
      Math[darker ? 'max' : 'min'](
        parseInt(decimal[2], 10) + difference,
        darker ? 0 : 255
      ).toString(16),
      2
    )
  ].join('')
}
const lightenColor = (color, ratio) => changeColor(color, ratio, false)
const darkenColor = (color, ratio) => changeColor(color, ratio, true)


/*
 * EXPORTS
 */
export { lightenColor, darkenColor }
