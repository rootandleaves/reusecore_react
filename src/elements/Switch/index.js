/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props Handler.


/*
 * SIBLINGS
 */
import { useToggle } from '../../hooks'


/*
 * STYLES
 */
import SwitchStyle from './index.style'


/*
 * OBJECTS
 */
const Switch = ({
  className,
  switchColor,
  isChecked,
  labelText,
  labelPosition,
  switchSize,
  isMaterial,
  barColor,
  onChange,
  onFocus,
  onBlur,
  handleOnChange,
  ...props
}) => {
  // Use toggle hooks
  const [toggleValue, toggleHandler] = useToggle(isChecked)

  // Add all class to an array
  const _addAllClasses = ['reusecore__switch']

  // Add label position class
  if (labelPosition) {
    _addAllClasses.push(`label_${labelPosition}`)
  }

  // IsMaterial prop checking
  if (isMaterial) {
    _addAllClasses.push('is-material')
  }

  // ClassName prop checking
  if (className) {
    _addAllClasses.push(className)
  }

  // Click event handler.
  handleOnChange = event => {
    toggleHandler()
    onChange(!toggleValue)
  }

  // Const assignment.
  const _labelField = labelText && (
    <span className='reusecore__field-label'>{labelText}</span>
  )
  const _position = labelPosition || 'top'

  // Return component.
  return (
    <SwitchStyle
      className={_addAllClasses.join(' ')}
      switchColor={switchColor}
      switchSize={switchSize}
      barColor={barColor}
      {...props}
    >
      <label>
        {'left' === _position || 'right' === _position || 'top' === _position ? _labelField : ''}

        <input
          checked={toggleValue}
          onChange={handleOnChange}
          onBlur={onBlur}
          onFocus={onFocus}
          className='switch'
          type='checkbox'
          value={toggleValue}
        />
        <div>
          <div />
        </div>
        {'bottom' === _position && _labelField}
      </label>
    </SwitchStyle>
  )
}


/*
 * PROPTYPES
 */
Switch.propTypes = {
  /** ClassName of the Switch */
  className: PropTypes.string,

  /** Add Material effect */
  isMaterial: PropTypes.bool,

  /** LabelText of the switch field */
  labelText: PropTypes.string,

  /** SwitchSize control switch width and height */
  switchSize: PropTypes.string,

  /** Set label position of the switch field */
  labelPosition: PropTypes.oneOf(['top', 'bottom', 'left', 'right']),

  /** Switch toggle state based on isChecked prop */
  isChecked: PropTypes.bool,

  /** Set color for Switch */
  SwitchColor: PropTypes.string,

  /** Set material bar color for Switch */
  barColor: PropTypes.string,

  /**
   * @ignore
   */
  onBlur: PropTypes.func,

  /**
   * @ignore
   */
  onFocus: PropTypes.func,

  /**
   * Callback fired when the value is changed.
   *
   * @param {object} event The event source of the callback.
   * You can pull out the new value by accessing `event.target.value`.
   */
  onChange: PropTypes.func
}
Switch.defaultProps = {
  isChecked: false,
  labelPosition: 'top',
  onBlur() {
    // Body
  },
  onFocus() {
    // Body
  },
  onChange() {
    // Body
  }
}


/*
 * EXPORTS
 */
export default Switch
