/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props handler.
import styled from 'styled-components' // NPM: React.js styled-components Library.


/*
 * SIBLINGS
 */
import { base, themed } from '../base'


/*
 * OBJECTS
 */
const LinkWrapper = styled('a')(
  { textDecoration: 'none' },
  base,
  themed('Link')
)
const Link = ({ children, ...props }) => (
  <LinkWrapper {...props}>{children}</LinkWrapper>
)


/*
 * PROPTYPES
 */
Link.propTypes = {
  as: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  children: PropTypes.any.isRequired,
  ...base.propTypes
}
Link.defaultProps = {
  as: 'a',
  m: 0,
  display: 'inline-block'
}


/*
 * EXPORTS
 */
export default Link // Exports
// Props Setting

