/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props Handler.
import Icon from 'react-icons-kit' // NPM: Next.js router library.
import CreateAbleReactSelect from 'react-select/async-creatable' // NPM: React.js ReactSelect Library.
import _ from 'underscore' // NPM: utility module.
import ReactSelect, { components } from 'react-select' // NPM: React.js ReactSelect Library.
import { useState, useEffect } from 'react' // NPM: React.js Library.
import { chevronUp } from 'react-icons-kit/ionicons/chevronUp' // NPM: react vector icons library.
import { chevronDown } from 'react-icons-kit/ionicons/chevronDown' // NPM: react vector icons library.
import { androidDone } from 'react-icons-kit/ionicons/androidDone' // NPM: react vector icons library.
import { androidClose } from 'react-icons-kit/ionicons/androidClose' // NPM: react vector icons library.


/*
 * STYLES
 */
import SelectStyle, { DoubleSelectWrapper } from './index.style'


/*
 * GLOBALS
 */
const _lastValue = new Map()


/*
 * OBJECTS
 */
const Select = ({
  className,
  value,
  hasDependentSelect,
  dependentSelectPlaceholder,
  dependentSelectValue,
  labelPosition,
  onDependentSelectOptions,
  onDependentSelectChange,
  onChange,
  ...props
}) => {
  // Use toggle hooks
  const [defaultValue, setDefaultValue] = useState(value ?? _lastValue.get(props.name) ?? [])
  const [dependentSelectDefaultValue, setDependentSelectDefaultValue] = useState(void 0)
  const [dependentSelectChildValue, setDependentSelectChildValue] = useState(void 0)

  // Add all classes to an array
  const _addAllClasses = ['reusecore__select']

  // Add label position class
  if (labelPosition) {
    _addAllClasses.push(`label_${labelPosition}`)
  }

  // ClassName prop checking
  if (className) {
    _addAllClasses.push(className)
  }

  // Handle input value
  const HandleOnChange = e => {
    // Update default value.
    setDefaultValue(e)

    // Update map with value.
    _lastValue.set(props.name, e)

    /*
     * Update dependent select value
     * if it is available.
     */
    hasDependentSelect && setDependentSelectDefaultValue(e)

    // Run on change handler.
    onChange(e)
  }
  const HandleOnCreate = e => {
    // Variable assignment.
    e = {
      'value': e,
      'label': e
    }

    // Const assignment.
    const _value = _.flatten(_.compact([props.isMulti ? defaultValue : void 0, e]))

    // Update default value.
    setDefaultValue(_value)

    // Update map with value.
    _lastValue.set(props.name, _value)

    // Run on change handler.
    onChange(_value)
  }

  // Component assignment.
  const DropdownIndicator = __props => (
    components.DropdownIndicator && (
      <components.DropdownIndicator {...__props}>
        <Icon style={__props.hasValue ? { 'color': '#000' } : void 0} icon={__props.hasValue ? androidDone : __props.selectProps.menuIsOpen ? chevronDown : chevronUp} size={!__props.hasValue ? 14 : void 0}/>
      </components.DropdownIndicator>
    )
  )
  const ClearIndicator = __props => (
    components.ClearIndicator && (
      <components.ClearIndicator {...__props}>
        <Icon icon={androidClose} />
      </components.ClearIndicator>
    )
  )

  // Event listener.
  useEffect(() => () => { _lastValue.delete(props.name) }, [])

  // Return component.
  return hasDependentSelect ? (
    <DoubleSelectWrapper className='doubleSelectWrapper'>
      <ReactSelect
        {...props}
        className='select-field__wrapper'
        classNamePrefix='select'
        components={{ DropdownIndicator, ClearIndicator }}
        onChange={HandleOnChange}
        value={defaultValue}
        defaultValue={defaultValue}
        theme={theme => ({
          ...theme,
          'borderRadius': 0,
          'colors': {
            ...theme.colors,
            'primary25': 'neutral50',
            'primary': 'black'
          }
        })}
      />
      <ReactSelect
        {...props}
        className='select-field__wrapper'
        classNamePrefix='select'
        placeholder={dependentSelectPlaceholder}
        defaultOptions={onDependentSelectOptions?.(dependentSelectDefaultValue)}
        options={onDependentSelectOptions?.(dependentSelectDefaultValue)}
        value={dependentSelectChildValue ?? dependentSelectValue}
        defaultValue={dependentSelectChildValue ?? dependentSelectValue}
        components={{ DropdownIndicator, ClearIndicator }}
        onChange={e => { setDependentSelectChildValue(e) ;onDependentSelectChange(e) }}
        theme={theme => ({
          ...theme,
          'borderRadius': 0,
          'colors': {
            ...theme.colors,
            'primary25': 'neutral50',
            'primary': 'black'
          }
        })}
      />
    </DoubleSelectWrapper>
  ) : (
    <SelectStyle className={_addAllClasses.join(' ')}>
      {
        props.isCreateAble ? (
          <CreateAbleReactSelect
            {...props}
            components={{ DropdownIndicator, ClearIndicator }}
            className='select-field__wrapper'
            classNamePrefix='select'
            value={defaultValue}
            defaultValue={defaultValue}
            onChange={HandleOnChange}
            onCreateOption={HandleOnCreate}
            theme={theme => ({
              ...theme,
              'borderRadius': 0,
              'colors': {
                ...theme.colors,
                'primary25': 'neutral50',
                'primary': 'black'
              }
            })}
          />
        ) : (
          <ReactSelect
            {...props}
            className='select-field__wrapper'
            classNamePrefix='select'
            components={{ DropdownIndicator, ClearIndicator }}
            onChange={HandleOnChange}
            value={defaultValue}
            defaultValue={defaultValue}
            theme={theme => ({
              ...theme,
              'borderRadius': 0,
              'colors': {
                ...theme.colors,
                'primary25': 'neutral50',
                'primary': 'black'
              }
            })}
          />
        )
      }
    </SelectStyle>
  )
}


/*
 * PROPTYPES
 */
Select.propTypes = {
  /**
   * You can add your custom class for select wrapper component.
   * note: We manual add react-select className and classNamePrefix props value
   */
  'className': PropTypes.string
}
Select.defaultProps = {}


/*
 * EXPORTS
 */
export default Select
