/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.
import themeGet from '@styled-system/theme-get' // NPM: styled-system themeGet library.


/*
 * OBJECTS
 */
const BaseStyle = styled.div`
  & .select__control {
    font-size: 17px;
    font-weight: 400;
    padding: 0 5px;
    border-radius: 9px;
    color: ${themeGet('colors.black')};
    background-color: ${themeGet('colors.lightDarkWhite')};
    border: none;
    & input {
      border-radius: 0 !important;
    }
    & .select__indicator-separator {
      display: none !important;
    }

    & .select__placeholder {
      color: ${themeGet('colors.black')};

      & .select__indicator {
        color: ${themeGet('colors.darkWhite')};
        border: none;
      }
    }

    & * {
      font-size: 16px;
    }
  }
  & .select__multi-value {
    background-color: ${themeGet('colors.black')};
    padding: 0 10px;
    border-radius: 9px;
    color: ${themeGet('colors.white')};
    & > .select__multi-value__label {
      font-weight: 500;
      color: ${themeGet('colors.white')};
    }
  }
  & .select__control--is-focused {
    box-shadow: none;
  }
  & .select__option {
    padding: 15px;
  }
  & .select__single-value {
    width: 100%;
  }
  & .select__menu {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    position: relative;
    top: -7px;
    margin: 0;
    overflow: hidden;
    background-color: ${themeGet('colors.lightDarkWhite')};
    box-shadow: none;
    border-radius: 0 0 9px 9px;
    .select__menu__option {
      border-radius: 9px;

      &:hover {
        box-shadow: none;
        background-color: ${themeGet('colors.black')};
      }
    }
    & .select__menu-list {
      padding: 0;
    }
  }
  & .select__value-container {
    padding: 10px;

    & .select__single-value {
      padding: 10px 0;
    }
  }
  & .select__input {
    & input {
      margin: 0;
      padding: 10px;
    }
  }
`
const DoubleSelectWrapper = styled(BaseStyle)`
  width: 100%;
  display: flex;
  justify-content: space-between;
  gap: 20px;
  & > * {
    width: 100%;
  }
`
const SelectStyle = styled(BaseStyle)`
  /* Select label default style */
  & .reusecore__field-label {
    color: ${themeGet('colors.black', '#767676')};
    font-size: ${themeGet('fontSizes.4', '16')}px;
    font-weight: ${themeGet('fontWeights.4', '500')};
  }
  &.label_left {
    display: flex;
    align-items: center;

    .reusecore__field-label {
      margin-right: ${themeGet('space.3', '10')}px;
    }
  }
  &.label_right {
    display: flex;
    flex-direction: row-reverse;
    align-items: center;

    .reusecore__field-label {
      margin-left: ${themeGet('space.3', '10')}px;
    }
  }
  &.label_top {
    .reusecore__field-label {
      display: flex;
      margin-bottom: ${themeGet('space.2', '8')}px;
    }
  }
  &.label_bottom {
    .reusecore__field-label {
      display: flex;
      margin-top: ${themeGet('space.2', '8')}px;
    }
  }
`


/*
 * PROPTYPES
 */
SelectStyle.displayName = 'SelectStyle'
SelectStyle.defaultProps = {
  as: 'div'
}


/*
 * EXPORTS
 */
export default SelectStyle
export {
  DoubleSelectWrapper
}
