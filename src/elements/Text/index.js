/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props Handler.
import styled from 'styled-components' // NPM: React.js styled-components Library
import {
  fontFamily,
  fontWeight,
  letterSpacing,
  lineHeight,
  textAlign
} from 'styled-system' // NPM: React.js styled-system Library


/*
 * SIBLINGS
 */
import Box from '../Box'
import { base, themed } from '../base'


/*
 * OBJECTS
 */
const TextWrapper = styled('p')(
  base,
  fontFamily,
  fontWeight,
  textAlign,
  lineHeight,
  letterSpacing,
  { 'margin': 0, 'padding': 0 },
  themed('Text')
)
const TextContainer = styled(Box)`
  display: flex;
  align-items: center;
`
const Text = ({ as, content, coloredContent, children, highLightColor, ...props }) =>
  coloredContent ? (
    <TextContainer {...props}>
      <TextWrapper as={as}>{content}{children}</TextWrapper>
      <TextWrapper {...{ 'color': highLightColor ?? 'primary' }} as={as}>{coloredContent}</TextWrapper>
    </TextContainer>) : (<TextContainer {...props}><TextWrapper as={as}>{content}{children}</TextWrapper></TextContainer>)


/*
 * PROPTYPES
 */
Text.propTypes = {
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  as: PropTypes.string,
  mt: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  mb: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  fontFamily: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  fontWeight: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  textAlign: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  lineHeight: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  letterSpacing: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  ...base.propTypes
}
Text.defaultProps = {
  as: 'p',
  mt: 0,
  mb: '1rem'
}


/*
 * EXPORTS
 */
export default Text
