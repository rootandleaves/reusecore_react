/*
 * IMPORTS
 */
import 'rheostat/initialize' // NPM: React.js Rheostat Initialization Library
import PropTypes from 'prop-types' // NPM: Props Handler
import Rheostat from 'rheostat' // NPM: React.js Rheostat Library
import { Fragment } from 'react' // NPM: React.js Library


/*
 * STYLES
 */
import RangeBoxStyle from './index.style'


/*
 * OBJECTS
 */
const HandleChange = props => {
  console.log(props, 'current range value')
}
const RangeBox = ({ className, labelText, labelPosition, type, ...props }) => {
  const { min, max } = props
  let initValue, lastValue

  // eslint-disable-next-line prefer-const
  initValue = min ? min : 0
  // eslint-disable-next-line prefer-const
  lastValue = max ? max : 100

  // Add all class to an array
  const _addAllClasses = ['reusecore__rangebox']
  // Add label position class
  if (labelPosition) {
    _addAllClasses.push(`label_${labelPosition}`)
  }
  // Label control
  const _position = labelPosition || 'right'
  const _LabelField = labelText && (
    <span className='reusecore__field-label'>{labelText}</span>
  )
  // ClassName prop checking
  if (className) {
    _addAllClasses.push(className)
  }

  // Return component.
  return (
    <Fragment>
      <RangeBoxStyle className={_addAllClasses.join(' ')}>
        <label>
          {'left' === _position || 'right' === _position || 'top' === _position ? _LabelField : ''}
          <Rheostat
            min={initValue}
            max={lastValue}
            values={[initValue, lastValue]}
            onChange={HandleChange}
          />
          <div>
            <div />
          </div>
          {'bottom' === _position && _LabelField}
        </label>
      </RangeBoxStyle>
    </Fragment>
  )
}


/*
 * PROPTYPES
 */
RangeBox.propTypes = {
  /** LabelText of the range-box field */
  labelText: PropTypes.string,

  /** LabelText of the range-box field */
  labelPosition: PropTypes.oneOf(['top', 'bottom', 'right', 'left']),

  // /** type of the range-box  */
  // Type: PropTypes.oneOf(['range', 'slide']).isRequired,

  // /** toolitip-placement of the range-box [for type="range" only]  */
  // Placement: PropTypes.oneOf([
  //   'left',
  //   'right',
  //   'top',
  //   'bottom',
  //   'topLeft',
  //   'topRight',
  //   'bottomLeft',
  //   'bottomRight'
  // ]),

  /** Minimum value of the range-box field */
  min: PropTypes.number.isRequired,

  /** Maximum value of the range-box field */
  max: PropTypes.number.isRequired

  // /** Stepper value of the range-box field */
  // Stepper: PropTypes.number.isRequired,

  // /** Default value of the range-box field [for type="slide" only] */
  // SlideDefaultValue: PropTypes.number,

  // /** Default value of the range-box field [for type="range" only] */
  // RangeDefaultValue: PropTypes.array,

  // /** Unit value of the range-box tooltip field [for type="range" only] */
  // Unit: PropTypes.string,

  // /** dots of the range-box field  */
  // Dots: PropTypes.bool,

  // /** disabled of the range-box field */
  // Disabled: PropTypes.bool,

  // /** vertical of the range-box field */
  // Vertical: PropTypes.bool
}
RangeBox.defaultProps = {
  labelText: 'ReuseCore RangeBox',
  labelPosition: 'top',
  /*
   * Type: 'range',
   * disabled: false,
   */
  min: 0,
  max: 100
  /*
   * Stepper: 5
   * unit: ' BDT',
   * placement: 'top',
   * slideDefaultValue: 3,
   * rangeDefaultValue: [20, 50],
   * dots: true,
   * vertical: false
   */
}


/*
 * EXPORTS
 */
export default RangeBox
