/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props handler.


/*
 * STYLES
 */
import IndexStyle from './index.style'


/*
 * OBJECTS
 */
const NavBar = ({ className, children, navBarStyle, ...props }) => {
  // Add all class to an array
  const addAllClasses = ['reusecore__navBar']

  // ClassName prop checking
  if (className) addAllClasses.push(className)

  // Return component.
  return (
    <IndexStyle className={addAllClasses.join(' ')} {...props}>
      {children}
    </IndexStyle>
  )
}


/*
 * PROPTYPES
 */
NavBar.propTypes = {
  /** ClassName of the NavBar. Default class is reusecore__navBar*/
  className: PropTypes.string,

  /**
   * Used to render menu, logo, button or any component that
   * you want to show in navBar.
   */
  children: PropTypes.element,

  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))]),

  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))]),

  space: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))]),

  borderRadius: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number]))]),

  boxShadow: PropTypes.string,

  color: PropTypes.string,

  display: PropTypes.string,

  alignItems: PropTypes.string,

  justifyContent: PropTypes.string,

  flexDirection: PropTypes.string,

  flexWrap: PropTypes.string
}
NavBar.defaultProps = {}


/*
 * EXPORTS
 */
export default NavBar
