/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.
import {
  alignItems,
  borderRadius,
  boxShadow,
  color,
  display,
  flexDirection,
  flexWrap,
  height,
  justifyContent,
  space,
  width
} from 'styled-system' // NPM: Styled system Library.


/*
 * OBJECTS
 */
const NavBarStyle = styled.nav`
  display: flex;
  align-items: center;
  min-height: 56px;

  /* Style system supported prop */
  ${display}
  ${alignItems}
  ${justifyContent}
  ${flexDirection}
  ${flexWrap}
  ${width}
  ${height}
  ${color}
  ${space}
  ${boxShadow}
  ${borderRadius}
`


/*
 * PROPTYPES
 */
NavBarStyle.displayName = 'NavBarStyle'


/*
 * EXPORTS
 */
export default NavBarStyle
