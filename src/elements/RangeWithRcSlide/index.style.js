/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.


/*
 * OBJECTS
 */
const RangeBoxStyle = styled.div``


/*
 * PROPTYPES
 */
RangeBoxStyle.propTypes = {}
RangeBoxStyle.displayName = 'RangeBoxStyle'
RangeBoxStyle.defaultProps = {
  as: 'div'
}


/*
 * EXPORTS
 */
export default RangeBoxStyle
