/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props handler.


/*
 * STYLES
 */
import LoaderStyle from './index.style'


/*
 * OBJECTS
 */
const Loader = ({ loaderColor, className, ...props }) => {
  // Add all class to an array
  const _addAllClasses = ['reusecore__loader']

  // ClassName prop checking
  if (className) _addAllClasses.push(className)

  // Return component.
  return (
    <LoaderStyle
      className={_addAllClasses.join(' ')}
      loaderColor={loaderColor}
      {...props}
    />
  )
}


/*
 * PROPTYPES
 */
Loader.propTypes = {
  /** ClassName of the Loader */
  className: PropTypes.string,

  /** Set loader width in number || string */
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /** Set loader height in number || string */
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /** Set color for loader */
  loaderColor: PropTypes.string
}
Loader.defaultProps = {}


/*
 * EXPORTS
 */
export default Loader
