/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.
import { variant } from 'styled-system' // NPM: Styled system Library.


/*
 * SIBLINGS
 */
import { colorStyle } from '../../theme/customVariant'
import { AnimSpinner } from '../Animation'
import { base } from '../base'


/*
 * OBJECTS
 */
const LoaderStyle = styled.span`
  /* loader default style */
  display: inline-flex;
  width: 14px;
  height: 14px;
  border-radius: 50%;
  overflow: hidden;
  border-width: 2px;
  border-style: solid;
  border-color: ${props => (props.loaderColor ? props.loaderColor : '#000000')};
  border-top-color: transparent !important;

  /* animation goes here */
  ${AnimSpinner}
  /* Style system custom color variant */
  ${colorStyle}
  ${base}
`


/*
 * PROPTYPES
 */
LoaderStyle.propTypes = {
  ...variant.propTypes
}
LoaderStyle.displayName = 'LoaderStyle'


/*
 * EXPORTS
 */
export default LoaderStyle
