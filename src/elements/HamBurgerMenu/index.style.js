/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: styled-components library for react.js
import themeGet from '@styled-system/theme-get' // NPM: themeGet library for styled-system.
import {
  border,
  borderRadius,
  boxShadow,
  color,
  height,
  space,
  width
} from 'styled-system' // NPM: styled-system library.


/*
 * OBJECTS
 */
const HamburgMenuWrapper = styled.button`
  border: 0;
  background: transparent;
  width: 40px;
  height: 40px;
  cursor: pointer;
  position: relative;
  z-index: 10000;
  ${width}
  ${height}
  ${color}
  ${space}
  ${border}
  ${boxShadow}
  ${borderRadius}
  .buttonBackground {
    background-color: ${themeGet('colors.primary')};
    border-radius: 100%;
    width: 40px;
    height: 40px;
    left: 0;
    top: 0;
    position: absolute;
    &::before {
      content: ' ';
      position: absolute;
      left: 0;
      top: 0;
      width: 40px;
      height: 40px;
      border-radius: 100%;
      background-color: ${themeGet('colors.primary')};
    }
  }
  &.isLight {
    .buttonBackground {
      &::before {
        display: none;
      }
    }
  }
  > span {
    display: block;
    width: 100%;
    height: 2px;
    margin: 4px 0;
    float: right;
    background-color: ${themeGet('colors.primary')};
    &:first-child {
      margin-top: 0;
    }
    &:last-child {
      width: calc(100% - 10px);
      margin-bottom: 0;
    }
  }
  &:focus,
  &:hover {
    outline: none;
    > span {
      &:last-child {
        width: 100%;
      }
    }
  }

  &:focus,
  &.active {
    > span {
      &:first-child {
        transform: rotate(45deg);
        transform-origin: 8px 50%;
      }
      &:nth-child(2) {
        display: none;
      }
      &:last-child {
        width: 100%;
        transform: rotate(-45deg);
        transform-origin: 9px 50%;
      }
    }
  }
`


/*
 * PROPERTIES
 */
HamburgMenuWrapper.displayName = 'HamburgMenuWrapper'


/*
 * EXPORTS
 */
export default HamburgMenuWrapper
