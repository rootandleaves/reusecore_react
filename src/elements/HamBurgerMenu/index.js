/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: react.js proptypes library.


/*
 * STYLES
 */
import HamburgMenuWrapper from './index.style'


/*
 * OBJECTS
 */
const HamburgMenu = ({ className, wrapperStyle, barColor, linkLight, ...props }) => {
  // Add all class to an array
  const _addAllClasses = ['hamburgMenu__bar']

  // ClassName prop checking
  if (className) _addAllClasses.push(className)
  if (linkLight) _addAllClasses.push('isLight')

  // Return component.
  return (
    <HamburgMenuWrapper
      className={_addAllClasses.join(' ')}
      {...wrapperStyle}
      aria-label='hamburgMenu'
      {...props}
    >
      <div className='buttonBackground' />
    </HamburgMenuWrapper>
  )
}


/*
 * PROPTYPES
 */
HamburgMenu.propTypes = {
  /** ClassName of the Hamburg menu. */
  className: PropTypes.string,

  /** BarColor allow to change hambrug menu's bar color. */
  barColor: PropTypes.string,

  /** WrapperStyle prop allow to change Hamburg menu bg color, width, height, space, boxShadow, border and borderRadius.*/
  wrapperStyle: PropTypes.object
}


/*
 * EXPORTS
 */
export default HamburgMenu
