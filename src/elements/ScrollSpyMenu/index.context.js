/*
 * IMPORTS
 */
import { createContext, useReducer } from 'react' // NPM: React.js Library.


/*
 * GLOBALS
 */
const initialState = {
  isOpen: false
}


/*
 * OBJECTS
 */
const DrawerContext = createContext({})
const DrawerProvider = ({ children }) => {
  // Hook assignment.
  const [state, dispatch] = useReducer((__state, __action) => {
    switch (__action.type) {
    case 'TOGGLE':
      return {
        ...__state,
        'isOpen': !__state.isOpen
      }
    default:
      return state
    }
  }, initialState)

  // Return Drawer Context.
  return (
    <DrawerContext.Provider value={{ state, dispatch }}>
      {children}
    </DrawerContext.Provider>
  )
}


/*
 * EXPORTS
 */
export default DrawerContext
export { DrawerProvider }
