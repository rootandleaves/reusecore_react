/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: styled-components library for react.js
import themeGet from '@styled-system/theme-get' // NPM: styled components themeGet library.


/*
 * OBJECTS
 */
const ScrollSpyWrapper = styled.div`
  height: 100%;
  background-color: ${themeGet('colors.black')};
  border-left: 15px solid ${themeGet('colors.secondary')} !important;
  & .header {
    display: flex;
    align-items: center;
    padding: 20px;
    background-color: ${themeGet('colors.black')};
    & .profilePicture {
      width: 50px;
      height: 50px;
      overflow: hidden;
      border-radius: 100%;
    }
    & .greetingsWrapper {
      margin-left: 10px;
      position: relative;
      top: -2.5px;
    }
    & .greetingsWrapperHeading {
      margin: 0;
      color: ${themeGet('colors.white')};
      font-size: 15px;
      text-transform: uppercase;
    }
    & .greetingsWrapperDescription {
      padding: 2.5px 10px;
      border-radius: 2.5px;
      font-size: 13px;
      text-transform: capitalize;
      text-align: center;
      margin: 0;
      color: ${themeGet('colors.white')};
      background-color: ${themeGet('colors.secondary')};
    }
  }
`
const AnchorLinkWrapper = styled.li`
  position: relative;
  height: 100%;
  display: flex;
  flex-direction: column;
  & .anchorLink {
    padding: 30px;
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    border-bottom: 2px solid ${themeGet('colors.lightestBlack')};
    color: ${themeGet('colors.white')};
    transition: all 0.25s ease-in-out;
    &:hover {
      background-color: ${themeGet('colors.lightestBlack')};
    }
  }
`


/*
 * EXPORTS
 */
export default ScrollSpyWrapper
export {
  AnchorLinkWrapper
}

