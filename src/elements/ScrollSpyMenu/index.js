/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props handler.
import ScrollSpy from 'react-scrollspy' // NPM: React scroll spy library.
import AnchorLink from 'react-anchor-link-smooth-scroll' // NPM: React anchor link smooth scroll.
import { useContext } from 'react' // NPM: React.js Library.


/*
 * STYLES
 */
import ScrollSpyWrapper, { AnchorLinkWrapper } from './index.style'


/*
 * CONTEXT
 */
import DrawerContext from './index.context'


/*
 * OBJECTS
 */
const Index = ({ className, account = {}, menuItems, drawerClose, ...props }) => {
  // Hook assignment.
  const { dispatch } = useContext(DrawerContext)

  // Empty array for scrollspy items
  const scrollItems = []

  // Convert menu path to scrollspy items
  menuItems.forEach(__item => scrollItems.push(__item.path.slice(1)))

  // Add all class to an array
  const addAllClasses = ['scrollspy__menu']

  // ClassName prop checking
  if (className) addAllClasses.push(className)

  // Close drawer when click on menu item
  const toggleDrawer = () => {
    dispatch({
      'type': 'TOGGLE'
    })
  }


  // Return component.
  return (
    <ScrollSpyWrapper>
      <ScrollSpy
        items={scrollItems}
        className={addAllClasses.join(' ')}
        drawerClose={drawerClose}
        {...props}>
        <div className='header'>
          <img className='profilePicture' src={account.profilePicture?.createReadStream?.path} alt='user profile picture' />
          <div className='greetingsWrapper'>
            <h3 className='greetingsWrapperHeading'>{account.fullName}</h3>
            <h3 className='greetingsWrapperDescription'>Welcome</h3>
          </div>
        </div>
        {menuItems.map((menu, index) => (
          <AnchorLinkWrapper key={`menu-item-${index}`}>
            {menu.staticLink ? (
              <AnchorLink className='anchorLink' href={menu.path}>{menu.label}</AnchorLink>
            ) : (
              <>
                {drawerClose ? (
                  <AnchorLink
                    href={menu.path}
                    offset={menu.offset}
                    onClick={toggleDrawer}
                  >
                    {menu.label}
                  </AnchorLink>
                ) : (
                  <a className='anchorLink' href={menu.path} offset={menu.offset}>{menu.label}</a>
                )}
              </>
            )}
          </AnchorLinkWrapper>
        ))}
      </ScrollSpy>
    </ScrollSpyWrapper>
  )
}


/*
 * PROPTYPES
 */
Index.propTypes = {
  /** ClassName of the Index. */
  'className': PropTypes.string,

  /**
   * MenuItems is an array of object prop which contain your menu
   * data.
   */
  'menuItems': PropTypes.array.isRequired,

  /** Class name that apply to the navigation element paired with the content element in viewport. */
  'currentClassName': PropTypes.string,

  /** Class name that apply to the navigation elements that have been scrolled past [optional]. */
  'scrolledPastClassName': PropTypes.string,

  /** HTML tag for ScrollSpy component if you want to use other than <ul/> [optional]. */
  'componentTag': PropTypes.string,

  /** Style attribute to be passed to the generated <ul/> element [optional]. */
  'style': PropTypes.object,

  /** Offset value that adjusts to determine the elements are in the viewport [optional]. */
  'offset': PropTypes.number,

  /** Name of the element of scrollable container that can be used with querySelector [optional]. */
  'rootEl': PropTypes.string,

  /**
   * Function to be executed when the active item has been updated [optional].
   */
  'onUpdate': PropTypes.func
}
Index.defaultProps = {
  'componentTag': 'ul',
  'currentClassName': 'is-current'
}


/*
 * EXPORTS
 */
export default Index
