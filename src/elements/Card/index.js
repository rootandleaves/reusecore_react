/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props type library.
import styled from 'styled-components' // NPM: Styled components Library.
import {
  backgroundImage,
  backgroundPosition,
  backgroundRepeat,
  backgroundSize,
  borderColor,
  borderRadius,
  borders,
  boxShadow,
  opacity
} from 'styled-system' // NPM: Style-system library.


/*
 * SIBLINGS
 */
import { cards } from '../../theme/customVariant'
import { base, themed } from '../base'


/*
 * OBJECTS
 */
const CardWrapper = styled('div')(
  base,
  borders,
  borderColor,
  borderRadius,
  boxShadow,
  backgroundImage,
  backgroundSize,
  backgroundPosition,
  backgroundRepeat,
  opacity,
  cards,
  themed('Card')
)
const Card = ({ children, ...props }) => (
  <CardWrapper {...props}>{children}</CardWrapper>
)


/*
 * PROPTYPES
 */
Card.propTypes = {
  children: PropTypes.any,
  ...borders.propTypes,
  ...borderColor.propTypes,
  ...borderRadius.propTypes,
  ...boxShadow.propTypes,
  ...backgroundImage.propTypes,
  ...backgroundSize.propTypes,
  ...backgroundPosition.propTypes,
  ...backgroundRepeat.propTypes,
  ...opacity.propTypes,
  ...cards.propTypes
}
Card.defaultProps = {
  boxShadow: '0px 20px 35px rgba(0, 0, 0, 0.05)'
}


/*
 * EXPORTS
 */
export default Card
