/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Proptypes handler.
import styled from 'styled-components' // NPM:React.js styled-components library.
import _ from 'underscore' // NPM: utility module.
import {
  fontFamily,
  fontWeight,
  letterSpacing,
  lineHeight,
  textAlign
} from 'styled-system' // NPM: react.js styled-system library.
import { motion } from 'framer-motion' // NPM: react motion library.


/*
 * SIBLINGS
 */
import Box from '../Box'
import { base, themed } from '../base'


/*
 * OBJECTS
 */
const HeadingWrapper = styled(motion.h1)(
  base,
  fontFamily,
  fontWeight,
  textAlign,
  lineHeight,
  letterSpacing,
  { 'margin': 0, 'padding': 0 },
  themed('Heading')
)
const HeadingContainer = styled(Box)`
  display: flex;
  align-items: center;
`
const Heading = ({ content, coloredContent, highLightColor, ...props }) =>
  coloredContent ? (
    <HeadingContainer {..._.omit(props, 'as')}>
      <HeadingWrapper>{content}</HeadingWrapper>
      <HeadingWrapper {...{ 'color': highLightColor ?? 'primary' }}>{coloredContent}</HeadingWrapper>
    </HeadingContainer>
  ) : (<HeadingWrapper {...props}>{content}</HeadingWrapper>)


/*
 * PROPTYPES
 */
Heading.propTypes = {
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  as: PropTypes.oneOf([
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6'
  ]),
  mt: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  mb: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  fontFamily: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  fontWeight: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  textAlign: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  lineHeight: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  letterSpacing: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ]),
  ...base.propTypes
}
Heading.defaultProps = {
  as: 'h2',
  mt: 0,
  mb: '1rem',
  fontWeight: 'bold'
}


/*
 * EXPORTS
 */
export default Heading

