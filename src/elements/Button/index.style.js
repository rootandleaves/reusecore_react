/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.
import { alignItems, boxShadow, variant } from 'styled-system' // NPM: Styled system Library.
import themeGet from '@styled-system/theme-get' // NPM: styled system themeGet library.


/*
 * SIBLINGS
 */
import { buttonStyle, colorStyle, sizeStyle } from '../../theme/customVariant'
import { base } from '../base'


/*
 * OBJECTS
 */
const ButtonStyle = styled.button`
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  color: ${themeGet('colors.white', '#ffffff')};
  font-family: inherit;
  text-decoration: none;
  text-transform: capitalize;
  transition: all 0.3s ease;
  span.btn-text {
    padding-left: ${themeGet('space.1', '4')}px;
    padding-right: ${themeGet('space.1', '4')}px;
  }
  span.btn-icon {
    display: flex;
    > div {
      display: flex !important;
    }
  }

  &:focus {
    outline: none;
  }

  &.disabled {
    opacity: 0.5;
    cursor: default;
  }
  /* Material style goes here */
  &.is-material {
    box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);
  }

  /* When button on loading stage */
  &.is-loading {
    .btn-text {
      padding-left: ${themeGet('space.2', '8')}px;
      padding-right: ${themeGet('space.2', '8')}px;
    }
  }

  /* Style system support */
  ${alignItems}
  ${boxShadow}
  ${buttonStyle}
  ${colorStyle}
  ${sizeStyle}
  ${base}
`
ButtonStyle.displayName = 'ButtonStyle'


/*
 * PROPTYPES
 */
ButtonStyle.propTypes = {
  ...alignItems.propTypes,
  ...boxShadow.propTypes,
  ...variant.propTypes
}


/*
 * EXPORTS
 */
export default ButtonStyle
