/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props type library.
import { Fragment } from 'react' // NPM: React.js library.


/*
 * SIBLINGS
 */
import Loader from '../Loader'


/*
 * STYLES
 */
import ButtonStyle from './index.style'


/*
 * OBJECTS
 */
const Button = ({
  type,
  title,
  icon,
  disabled,
  iconPosition,
  onClick,
  loader,
  loaderColor,
  isMaterial,
  isLoading,
  className,
  ...props
}) => {
  // Add all class to an array
  const _addAllClasses = ['reusecore__button']

  // IsLoading prop checking
  if (isLoading) {
    _addAllClasses.push('is-loading')
  }

  // IsMaterial prop checking
  if (isMaterial) {
    _addAllClasses.push('is-material')
  }

  // Disable given button if props provided.
  if (disabled) {
    _addAllClasses.push('disabled')
  }

  // ClassName prop checking
  if (className) {
    _addAllClasses.push(className)
  }

  // Checking button loading state
  const buttonIcon = false === isLoading ? (
    icon && <span className='btn-icon'>{icon}</span>
  ) : (
    <Fragment>
      {loader ? loader : <Loader loaderColor={loaderColor || '#FFF'}/>}
    </Fragment>
  )

  // Set icon position
  const position = iconPosition || 'right'

  // Return component.
  return (
    <ButtonStyle
      type={type}
      className={_addAllClasses.join(' ')}
      icon={icon}
      disabled={disabled}
      icon-position={position}
      onClick={onClick}
      {...props}
    >
      {'left' === position && buttonIcon}
      {title && <span className='btn-text'>{title}</span>}
      {'right' === position && buttonIcon}
    </ButtonStyle>
  )
}


/*
 * PROPTYPES
 */
Button.propTypes = {
  /** ClassName of the button */
  className: PropTypes.string,

  /** Add icon */
  type: PropTypes.oneOf(['button', 'submit', 'reset']),

  /** Add icon */
  icon: PropTypes.object,

  /** Add loader */
  loader: PropTypes.object,

  /** Add Material effect */
  isMaterial: PropTypes.bool,

  /** Button Loading state */
  isLoading: PropTypes.bool,

  /** Button Loading state */
  loaderColor: PropTypes.string,

  /** If true button will be disabled */
  disabled: PropTypes.bool,

  /** Adjust your icon and loader position [if you use loader] */
  iconPosition: PropTypes.oneOf(['left', 'right']),

  /** Variant change button shape */
  variant: PropTypes.oneOf(['textButton', 'outlined', 'fab', 'extendedFab']),

  /**
   * Primary || secondary || warning || error  change text and border color.
   *  And primaryWithBg || secondaryWithBg || warningWithBg || errorWithBg change text, border and background color
   */
  colors: PropTypes.oneOf([
    'primary',
    'secondary',
    'warning',
    'error',
    'primaryWithBg',
    'secondaryWithBg',
    'warningWithBg',
    'errorWithBg'
  ]),

  /**
   * Gets called when the user clicks on the button
   */
  onClick: PropTypes.func
}
Button.defaultProps = {
  disabled: false,
  isMaterial: false,
  isLoading: false,
  type: 'button'
}


/*
 * EXPORTS
 */
export default Button
