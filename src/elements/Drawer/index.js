/*
 * IMPORTS
 */
import 'rc-drawer/assets/index.css' // NPM: rc-drawer CSS files.
import PropTypes from 'prop-types' // NPM: Proptypes handler.
import RcDrawer from 'rc-drawer' // NPM:React.js library.
import { Fragment } from 'react' // NPM: React.js Library.


/*
 * OBJECTS
 */
const Drawer = ({
  className,
  children,
  closeButton,
  closeButtonStyle,
  drawerHandler,
  toggleHandler,
  open,

  ...props
}) => {
  // Add all class to an array
  const _addAllClasses = ['reusecore__drawer']

  // ClassName prop checking
  if (className) _addAllClasses.push(className)

  // Return component.
  return (
    <Fragment>
      <RcDrawer
        open={open}
        onClick={toggleHandler}
        className={_addAllClasses.join(' ')}
        {...props}
      >
        <div
          className='reusecore-drawer__close'
          onClick={toggleHandler}
          style={closeButtonStyle}
        >
          {closeButton}
        </div>
        {children}
      </RcDrawer>
      <div
        className='reusecore-drawer__handler'
        style={{ display: 'inline-block' }}
        onClick={toggleHandler}
      >
        {drawerHandler}
      </div>
    </Fragment>
  )
}


/*
 * PROPTYPES
 */
Drawer.propTypes = {
  /** ClassName of the Drawer */
  className: PropTypes.string,

  /** Used to render icon, button, text or any elements inside the closeButton prop. */
  closeButton: PropTypes.element,

  /** Set drawer width. Default value is 300px. */
  width: PropTypes.string,

  /** Set drawer position left || right || top || bottom. */
  placement: PropTypes.oneOf(['left', 'right', 'top', 'bottom']),

  /** DrawerHandler could be button, icon, string or any component */
  drawerHandler: PropTypes.element.isRequired
}
Drawer.defaultProps = {
  width: '300px',
  borderLeft: '5px solid #323232',
  handler: false,
  level: null
}


/*
 * EXPORTS
 */
export default Drawer
