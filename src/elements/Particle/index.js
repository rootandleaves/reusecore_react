/*
 * IMPORTS
 */
import Particles from 'react-tsparticles' // NPM: react particles library.


/*
 * OBJECTS
 */
const ParticlesComponent = () => (
  <Particles
    className='particle'
    params={{
      'particles': {
        'number': {
          'value': 17,
          'density': { 'enable': true, 'value_area': 1200 }
        },
        'color': {
          'value': [
            '#FF5F6D',
            '#FFC371',
            '#FFFFFF',
            '#828282',
            '#626262'
          ],
          'random': true
        },
        'fpsLimit': 60,
        'interactivity': {
          'detectsOn': 'canvas',
          'modes': {
            'repulse': {
              'distance': 1000,
              'duration': 1000000
            }
          }
        },
        'shape': {
          'type': ['circle']
        },
        'opacity': {
          'random': false,
          'anim': { 'enable': false, 'speed': 10, 'opacity_min': 0.1, 'sync': false }
        },
        'size': {
          'value': 1.9,
          'random': false
        },
        'line_linked': {
          'enable': false
        },
        'move': {
          'enable': true,
          'speed': 0.4,
          'direction': 'none',
          'random': true,
          'straight': false,
          'bounce': true,
          'attract': { 'enable': true, 'rotateX': 100, 'rotateY': 400 }
        }
      },
      'retina_detect': true
    }}
  />
)


/*
 * EXPORTS
 */
export default ParticlesComponent
