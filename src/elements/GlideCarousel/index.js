/*
 * IMPORTS
 */
import '@glidejs/glide/dist/css/glide.core.min.css' // NPM: glide js css library.
import PropTypes from 'prop-types' // NPM: react proptypes library.
import Glide from '@glidejs/glide' // NPM: glide js library.
import { Fragment, useEffect } from 'react' // NPM: react.js library.


/*
 * STYLES
 */
import GlideWrapper, {
  BulletButton,
  BulletControlWrapper,
  ButtonControlWrapper,
  ButtonWrapper,
  DefaultBtn
} from './index.style'


/*
 * OBJECT
 */
const GlideCarousel = ({
  className,
  children,
  options,
  controls,
  prevButton,
  nextButton,
  prevWrapper,
  nextWrapper,
  bullets,
  numberOfBullets,
  buttonWrapperStyle,
  bulletWrapperStyle,
  bulletButtonStyle,
  carouselSelector
}) => {
  // Const assignment.
  const addAllClasses = ['glide']
  const totalBullets = []

  // ClassName prop checking
  if (className) addAllClasses.push(className)

  // Update number of bullets
  for (let i = 0 ;i < numberOfBullets ;i++) totalBullets.push(i)

  // Event handler.
  useEffect(() => {
    // Create new glide object and mount it.
    const glide = new Glide(
      carouselSelector ? `#${carouselSelector}` : '#glide',
      {
        ...options
      }
    )
    glide.mount()
  })

  // Return component.
  return (
    <GlideWrapper
      className={addAllClasses.join(' ')}
      id={carouselSelector || 'glide'}
    >
      <div className='glide__track' data-glide-el='track'>
        <ul className='glide__slides'>{children}</ul>
      </div>
      {controls && (
        <ButtonControlWrapper
          className='glide__controls'
          data-glide-el='controls'
          {...buttonWrapperStyle}
        >
          <ButtonWrapper
            {...prevWrapper}
            className='glide__prev--area'
            data-glide-dir='<'
            aria-label='prev'
          >
            {prevButton ? prevButton : void 0}
          </ButtonWrapper>
          <ButtonWrapper
            {...nextWrapper}
            className='glide__next--area'
            data-glide-dir='>'
            aria-label='next'
          >
            {nextButton ? nextButton : void 0}
          </ButtonWrapper>
        </ButtonControlWrapper>
      )}
      {bullets && (
        <BulletControlWrapper
          className='glide__bullets'
          data-glide-el='controls[nav]'
          {...bulletWrapperStyle}
        >
          <Fragment>
            {totalBullets.map(index => (
              <BulletButton
                key={index}
                className='glide__bullet'
                data-glide-dir={`=${index}`}
                aria-label={`bullet${index + 1}`}
                {...bulletButtonStyle}
              />
            ))}
          </Fragment>
        </BulletControlWrapper>
      )}
    </GlideWrapper>
  )
}


/*
 * PROPTYPES
 */
GlideCarousel.propTypes = {
  /** ClassName of the GlideCarousel. */
  'className': PropTypes.string,

  /** Children. */
  'children': PropTypes.oneOfType([PropTypes.array, PropTypes.object, PropTypes.element]),

  /** You can add your custom glide options using this prop. */
  'options': PropTypes.object,

  /** Hide || show controls nav. */
  'controls': PropTypes.bool,

  /** Hide || show bullets nav. */
  'bullets': PropTypes.bool,

  /** This prop only take your slider / carousel / testimonials data length. */
  'numberOfBullets': PropTypes.number,

  /**
   * BulletWrapperStyle is a bullet control wrapper style object prop.
   * It's contain display, space, alignItems,
   * justifyContent and flexWrap style-system prop.
   */
  'bulletWrapperStyle': PropTypes.object,

  /**
   * ButtonWrapperStyle is a button control wrapper style object prop.
   * It's contain same as buttonWrapperStyle style-system prop and
   * position, left, right, top and bottom.
   */
  'buttonWrapperStyle': PropTypes.object,

  /**
   * PrevWrapper is a previous button wrapper style object prop.
   * It's contain display, space, bg, borders, boxShadow, borderRadius,
   * position, top, left, right and bottom style-system prop.
   */
  'prevWrapper': PropTypes.object,

  /**
   * NextWrapper is a next button wrapper style object prop.
   * It's contain same as prevWrapper style-system prop.
   */
  'nextWrapper': PropTypes.object,

  /** Set previous button for glide carousel. */
  'prevButton': PropTypes.oneOfType([PropTypes.string, PropTypes.object]),

  /** Set next button for glide carousel. */
  'nextButton': PropTypes.oneOfType([PropTypes.string, PropTypes.object]),

  /**
   * BulletButtonStyle is a bullet button style object prop.
   * It's contain  display, width, height, space,
   * bg, borders, boxShadow and borderRadius style-system prop.
   */
  'bulletButtonStyle': PropTypes.object
}
GlideCarousel.defaultProps = {
  'controls': true,
  'bullets': false
}


/*
 * EXPORTS
 */
export default GlideCarousel
