/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props type library.


/*
 * STYLES
 */
import { GlideSlideWrapper } from '../index.style'


/*
 * OBJECTS
 */
const Index = ({ children }) => (
  <GlideSlideWrapper className='glide__slide'>{children}</GlideSlideWrapper>
)


/*
 * PROPTYPES
 */
Index.propTypes = {
  /** Children. */
  children: PropTypes.element
}


/*
 * EXPORTS
 */
export default Index
