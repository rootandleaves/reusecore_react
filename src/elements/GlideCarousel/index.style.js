/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: styled component library.
import {
  alignItems,
  borderRadius,
  borders,
  bottom,
  boxShadow,
  color,
  display,
  flexWrap,
  height,
  justifyContent,
  left,
  position,
  right,
  space,
  top,
  width
} from 'styled-system' // NPM: styled system library.


/*
 * OBJECTS
 */
const GlideWrapper = styled.div`
  ${width}
  ${height}
  ${space}
`
const GlideSlideWrapper = styled.li`
  ${space}
  ${color}
  ${borders}
  ${boxShadow}
  ${borderRadius}
`
const ButtonWrapper = styled.div`
  display: inline-block;
  ${display}
  ${space}
  ${color}
  ${borders}
  ${boxShadow}
  ${borderRadius}
  ${position}
  ${top}
  ${left}
  ${right}
  ${bottom}
`
const ButtonControlWrapper = styled.div`
  ${display}
  ${space}
  ${alignItems}
  ${justifyContent}
  ${position}
  ${top}
  ${left}
  ${right}
  ${bottom}
`
const BulletControlWrapper = styled.div`
  ${display}
  ${space}
  ${alignItems}
  ${justifyContent}
  ${flexWrap}
`
const BulletButton = styled.button`
  cursor: pointer;
  width: 10px;
  height: 10px;
  margin: 4px;
  border: 0;
  padding: 0;
  outline: none;
  border-radius: 50%;
  background-color: #d6d6d6;
  &:hover,
  &.glide__bullet--active {
    background-color: #869791;
  }
  ${display}
  ${space}
  ${color}
  ${borders}
  ${boxShadow}
  ${borderRadius}
  ${width}
  ${height}
`
const DefaultBtn = styled.button`
  cursor: pointer;
  margin: 10px 3px;
`


/*
 * OBJECTS
 */
export default GlideWrapper
export {
  GlideSlideWrapper,
  ButtonControlWrapper,
  ButtonWrapper,
  BulletControlWrapper,
  BulletButton,
  DefaultBtn
}
