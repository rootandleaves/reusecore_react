/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props type library.


/*
 * STYLES
 */
import AlertStyle from './index.style'


/*
 * OBJECTS
 */
const Alert = ({ className, isMaterial, children, ...props }) => {
  // Add all class to an array
  const _addAllClasses = ['reusecore__alert']

  // ClassName prop checking
  if (className) {
    _addAllClasses.push(className)
  }

  // IsMaterial prop checking.
  if (isMaterial) {
    _addAllClasses.push('is-material')
  }

  // Return component.
  return (
    <AlertStyle className={_addAllClasses.join(' ')} {...props}>
      {children}
    </AlertStyle>
  )
}


/*
 * PROPTYPES
 */
Alert.defaultProps = {}
Alert.propTypes = {
  /** ClassName of the Alert */
  className: PropTypes.string,

  /** Add Material effect */
  isMaterial: PropTypes.bool,

  /**
   * Used to render icon, button, text or any elements inside the Alert.
   * This can be a string or any component.
   */
  children: PropTypes.element
}


/*
 * EXPORTS
 */
export default Alert
