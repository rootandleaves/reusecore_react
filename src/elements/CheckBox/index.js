/*
 * IMPORTS
 */
import PropTypes from 'prop-types'// NPM: Props type library.
import CustomCheckBox from 'react-custom-checkbox' // NPM: react custom checkbox.
import { Icon } from 'react-icons-kit' // NPM: react icons kit library.
import { androidCheckbox } from 'react-icons-kit/ionicons/androidCheckbox' // NPM: Checkbox icon.
import { useState } from 'react'// NPM: React.js library.


/*
 * STYLES
 */
import CheckBoxStyle from './index.style'


/*
 * OBJECTS
 */
const CheckBox = ({
                    className,
                    isChecked,
                    labelText,
                    value,
                    noLabel,
                    borderColor,
                    id,
                    htmlFor,
                    labelPosition,
                    isMaterial,
                    disabled,
                    onChange,
                    ...props
                  }) => {
  // Use toggle hooks.
  const [toggleValue, toggleHandler] = useState(isChecked ?? value)


  // Add all class to an array
  const addAllClasses = ['reusecore__checkbox']

  // Add label position class
  if (labelPosition) {
    addAllClasses.push(`label_${labelPosition}`)
  }

  // IsMaterial prop checking
  if (isMaterial) {
    addAllClasses.push('is-material')
  }

  // ClassName prop checking
  if (className) {
    addAllClasses.push(className)
  }

  // Label control
  const LabelField = labelText && (
      <span className='reusecore__field-label'>{labelText}</span>
  )

  // Label default position.
  const position = labelPosition || 'right'

  // Return component.
  return (
      <CheckBoxStyle className={addAllClasses.join(' ')} {...props}>
        <label htmlFor={htmlFor}>
          {noLabel ? void 0 : 'left' === position || 'right' === position ? LabelField : ''}
          <CustomCheckBox
              style={{ 'position': 'relative' }}
              type='checkbox'
              className='checkbox'
              id={id}
              value={value}
              checked={toggleValue}
              icon={<Icon icon={androidCheckbox} size={25} className='icon' style={{ 'color': borderColor ?? '#000' }}/>}
              borderColor={borderColor ?? '#000'}
              borderRadius={3}
              onChange={() => { onChange(!toggleValue) ;toggleHandler(!toggleValue) }}
              disabled={disabled}
              {...props}
          />
        </label>
      </CheckBoxStyle>
  )
}


/*
 * PROPTYPES
 */
CheckBox.propTypes = {
  /** ClassName of the Checkbox */
  className: PropTypes.string,

  /** LabelText of the checkbox field */
  labelText: PropTypes.string,

  /** Hide label */
  noLabel: PropTypes.bool,

  /**
   * Note: id and htmlFor must be same.
   */
  htmlFor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /** Set checkbox id in number || string */
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /** Value of the checkbox field */
  value: PropTypes.bool,

  /** LabelText of the checkbox field */
  labelPosition: PropTypes.oneOf(['right', 'left']),

  /** Checkbox toggle state based on isChecked prop */
  isChecked: PropTypes.bool,

  /** Disabled of the checkbox field */
  disabled: PropTypes.bool
}
CheckBox.defaultProps = {
  isChecked: false,
  labelText: 'Checkbox label',
  labelPosition: 'right',
  disabled: false
}


/*
 * EXPORTS
 */
export default CheckBox
