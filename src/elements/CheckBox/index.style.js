/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.
import themeGet from '@styled-system/theme-get' // NPM: styled-system themeGet library.


/*
 * SIBLINGS
 */
import { base } from '../base'


/*
 * OBJECTS
 */
const CheckBoxStyle = styled.div`
  display: inline-flex;
  /* Switch label default style */
  .reusecore__field-label {
    color: ${themeGet('colors.white', '#FFFFFF')};
    padding: 2.5px 10px;
    text-transform: uppercase;
    text-decoration: none;
    border-radius: 25px;
    background-color: ${themeGet('colors.darkWhite', '#202020')};
    font-size: 10px;
  }
  & .icon {
    color: ${themeGet('colors.black')};
  }
  /* Switch label style when labelPosition on left */
  &.label_left {
    label {
      display: flex;
      align-items: center;
      .reusecore__field-label {
        margin-right: ${themeGet('space.3', '10')}px;
      }
    }
  }

  /* Switch label style when labelPosition on right */
  &.label_right {
    label {
      display: flex;
      flex-direction: row-reverse;
      align-items: center;

      .reusecore__field-label {
        margin-left: ${themeGet('space.3', '10')}px;
      }
    }
  }
  & svg {
    position: relative;
    top: -1px;
  }

  /* Checkbox default style */
  input[type='checkbox'] {
    .checkbox {
      opacity: 0;
      margin: 0;
      z-index: -1;
      overflow: hidden;
      pointer-events: none;
      & > div {
        background-color: ${themeGet('colors.darkWhite')} !important;
      }
      &:checked > div {
        border-color: ${themeGet('colors.darkWhite', '#028489')};
        background-color: ${themeGet('colors.darkWhite', '#028489')};
        &::after {
          opacity: 1;
          visibility: visible;
          transform: rotate(45deg) scale(1);
        }
      }
    }
  }

  /* support base component props */
  ${base}
`
CheckBoxStyle.displayName = 'CheckBoxStyle'

/*
 * PROPTYPES
 */
CheckBoxStyle.propTypes = {}


/*
 * EXPORTS
 */
export default CheckBoxStyle
