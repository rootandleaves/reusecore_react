/*
 * IMPORTS
 */
import { useEffect, useState } from 'react' // NPM: react.js library.


/*
 * STYLES
 */
import {
  DropdownHeading,
  DropdownMenuItemsWrapper,
  DropdownMenuItemWrapper,
  DropdownMenuWrapper
} from './index.style'


/*
 * OBJECTS
 */
const DropdownMenu = ({ content, show, dropdownItems, dropDownWrapperClass, dropDownHeadingClass, dropDownMenuClass, dropDownItemClass }) => {
  // Const assignment.
  const [menuState, SetMenuState] = useState({ show })

  // Adding listeners.
  useEffect(() => {
    // Event Listener.
    window.addEventListener('click', HandleDocumentClick)

    // Return component.
    return () => {
      // Event Listener.
      window.removeEventListener('click', HandleDocumentClick)
    }
  })

  // Event handler.
  const HandleToggle = () => {
    // Manage menu state.
    SetMenuState(prevState =>
    // Return state.
      ({
        ...menuState,
        show: !prevState.show
      }))
  }
  const HandleDocumentClick = () => {
    // On click menu show.
    if (menuState.show) {
      // Toggle Document on click.
      HandleToggle()
    }
  }

  // Return component.
  return (
    <DropdownMenuWrapper className={dropDownWrapperClass} onClick={e => e.stopPropagation()}>
      <DropdownHeading className={dropDownHeadingClass} onClick={HandleToggle}>{content}</DropdownHeading>
      {menuState.show && (<DropdownMenuItemsWrapper className={dropDownMenuClass}>
        {dropdownItems && dropdownItems.map((item, index) => (
          <DropdownMenuItemWrapper className={dropDownItemClass} key={index} onClick={HandleToggle}>
            {item}
          </DropdownMenuItemWrapper>
        ))}
      </DropdownMenuItemsWrapper>)}
    </DropdownMenuWrapper>
  )
}


/*
 * EXPORTS
 */
export default DropdownMenu
