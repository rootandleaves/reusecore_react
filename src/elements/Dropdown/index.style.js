/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: styled component library.


/*
 * PACKAGES
 */
import Box from '../Box'


/*
 * EXPORTS
 */
export const DropdownMenuWrapper = styled.div`
  position: relative;
  cursor: pointer;
  transition: 0.2s ease-in-out;
`
export const DropdownMenuItemsWrapper = styled.ul`
  margin-top: 40px;
  padding: 0;
  list-style: none;
  background-color: #000;
  position: absolute;
  top: 0;
  left: ${props => ('left' === props.dropdownDirection ? '0' : 'auto')};
  right: ${props => ('right' === props.dropdownDirection ? '0' : 'auto')};
  z-index: 15;
  min-width: 190px;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.16);
  border-radius: 3px;
`
export const DropdownMenuItemWrapper = styled.li`
  transition: background-color 0.3s ease-in-out;
  font-size: calc(0.30em + 1vmin);
  margin-top: 10px;
  padding: 10px 20px;
  a {
    display: block;
    color: #ffffff;
  }
`
export const DropdownHeading = styled(Box)`
  color: #ffffff;
  margin: 0 20px;
  font-weight: 600;
  font-size: calc(0.30em + 1vmin);
`
