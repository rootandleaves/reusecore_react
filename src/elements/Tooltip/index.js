/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: to check received props
import { useState } from 'react' // NPM: React.js Library


/*
 * STYLES
 */
import {
  BubbleSize,
  BubbleStyle,
  TooltipStyle,
  TriggerStyle
} from './index.style'


/*
 * OBJECTS
 */
const Tooltip = ({
  className,
  position,
  tooltipColor,
  bubbleSize,
  bubbleStyle,
  triggerStyle,
  message,
  children,
  ...props
}) => {
  // Tooltip local state
  const [state, setState] = useState({
    open: false
  })

  // Add all class to an array
  const _addAllClasses = ['reusecore__tooltip']

  // ClassName prop checking
  if (className) {
    _addAllClasses.push(className)
  }

  // Hide tooltip on mouse leave
  const HideTooltip = () => {
    setState({ open: false })
  }

  // Show tooltip on mouse over
  const ShowTooltip = () => {
    setState({ open: true })
  }

  // Return component.
  return (
    <TooltipStyle
      className={_addAllClasses.join(' ')}
      onMouseLeave={HideTooltip}
      tooltipColor={tooltipColor}
      {...props}
    >
      {state.open && (
        <BubbleSize
          className={`tooltip-bubble tooltip-${position}`}
          {...bubbleSize}
        >
          <BubbleStyle className='tooltip-message' {...bubbleStyle}>
            {message}
          </BubbleStyle>
        </BubbleSize>
      )}
      <TriggerStyle
        className='tooltip-trigger'
        onMouseOver={ShowTooltip}
        {...triggerStyle}
      >
        {children}
      </TriggerStyle>
    </TooltipStyle>
  )
}


/*
 * PROPTYPES
 */
Tooltip.propTypes = {
  /** ClassName of the Tooltip */
  className: PropTypes.string,

  /** Change tooltip tooltipColor */
  tooltipColor: PropTypes.string,

  /** TriggerStyle prop allow to change tooltip trigger fontSize, fontWeight, margin, padding, color and bg color.*/
  triggerStyle: PropTypes.object,

  /**
   * BubbleSize prop allow to change tooltip's buble width including min and max width.
   * And height including min and max height
   */
  BubbleSize: PropTypes.object,

  /** BubbleStyle prop allow to change tooltip's buble boxShadow, color, fontSize, borderRadius */
  bubbleStyle: PropTypes.object,

  /** Set tooltip position left || right || top || bottom. */
  position: PropTypes.oneOf(['left', 'right', 'top', 'bottom'])
}
Tooltip.defaultProps = {
  isChecked: false
}


/*
 * EXPORTS
 */
export default Tooltip
