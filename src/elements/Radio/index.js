/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props handler.


/*
 * SIBLINGS
 */
import { useToggle } from '../../hooks'


/*
 * STYLES
 */
import RadioBoxStyle from './index.style'


/*
 * OBJECTS
 */
const Radio = ({
  className,
  isChecked,
  labelText,
  value,
  id,
  htmlFor,
  isMaterial,
  labelPosition,
  disabled,
  ...props
}) => {
  // Use toggle hooks
  const [toggleValue, toggleHandler] = useToggle(isChecked)

  // Add all class to an array
  const _addAllClasses = ['reusecore__radio']

  // Add label position class
  if (labelPosition) {
    _addAllClasses.push(`label_${labelPosition}`)
  }

  // IsMaterial prop checking
  if (isMaterial) {
    _addAllClasses.push('is-material')
  }

  // ClassName prop checking
  if (className) {
    _addAllClasses.push(className)
  }

  const position = labelPosition || 'right'

  // Label control
  const LabelField = labelText && (
    <span className='reusecore__field-label'>{labelText}</span>
  )

  // Return component.
  return (
    <RadioBoxStyle className={_addAllClasses.join(' ')} {...props}>
      <label>
        {'left' === position || 'right' === position ? LabelField : ''}
        <input
          type='radio'
          className='radio'
          id={id}
          value={value}
          checked={toggleValue}
          onChange={toggleHandler}
          disabled={disabled}
          {...props}
        />
        <div />
      </label>
    </RadioBoxStyle>
  )
}


/*
 * PROPTYPES
 */
Radio.propTypes = {
  /** ClassName of the radio */
  className: PropTypes.string,

  /** LabelText of the radio field */
  labelText: PropTypes.string,

  /**
   * Note: id and htmlFor must be same.
   */
  htmlFor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /** Set radio id in number || string */
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /** Value of the radio field */
  value: PropTypes.string,

  /** LabelText of the radio field */
  labelPosition: PropTypes.oneOf(['right', 'left']),

  /** Radio toggle state based on isChecked prop */
  isChecked: PropTypes.bool,

  /** Disabled of the radio field */
  disabled: PropTypes.bool
}
Radio.defaultProps = {
  isChecked: false,
  labelText: 'Radio label',
  labelPosition: 'right',
  disabled: false
}


/*
 * EXPORTS
 */
export default Radio
