/*
 * IMPORTS
 */
import Link from 'next/link' // NPM: Replacement for <a> tag.
import { Fragment } from 'react' // NPM: React.js Library.


/*
 * STYLES
 */
import { ListWrapper } from './index.style'


/*
 * OBJECTS
 */
const List = ({ className, icon, text, link, ...props }) => (
  <ListWrapper className={className}>
    {link ? (
      <Link href={link}>
        <a>
          {icon}
          {text}
        </a>
      </Link>
    ) : (
      <Fragment>
        {icon}
        {text}
      </Fragment>
    )}
  </ListWrapper>
)


/*
 * EXPORTS
 */
export default List
