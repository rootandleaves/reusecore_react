/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.


/*
 * OBJECTS
 */
const ListWrapper = styled.div``


/*
 * EXPORTS
 */
export { ListWrapper }
