/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.
import ThemedStyleSheet from 'react-with-styles/lib/ThemedStyleSheet' // NPM: For registering your own Aphrodite interface.
import aphroditeInterface from 'react-with-styles-interface-aphrodite' // NPM: AphroditeInterface to style react components.
import DefaultTheme from 'react-dates/lib/theme/DefaultTheme' // NPM: Default theme.


/*
 * GLOBALS
 */
ThemedStyleSheet.registerInterface(aphroditeInterface)
ThemedStyleSheet.registerTheme(DefaultTheme)


/*
 * OBJECTS
 */
const DateRangePickerStyle = styled.div``


/*
 * PROPTYPES
 */
DateRangePickerStyle.propTypes = {}
DateRangePickerStyle.displayName = 'DateRangePickerStyle'
DateRangePickerStyle.defaultProps = {}


/*
 * EXPORTS
 */
export default DateRangePickerStyle
