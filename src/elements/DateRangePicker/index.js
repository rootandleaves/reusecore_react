/*
 * IMPORTS
 */
import 'react-dates/lib/css/_datepicker.css' // NPM: React.js datePicker css.
import PropTypes from 'prop-types' // NPM: Prop-types library.
import moment from 'moment' // NPM: Moment.js library.
import { DateRangePicker } from 'react-dates' // NPM: React.js date picker library.
import { Component, Fragment } from 'react' // NPM: React.js Library.


/*
 * STYLES
 */
import DateRangePickerStyle from './index.style'


/*
 * OBJECTS
 */
class DateRangePickerBox extends Component {
  // Constructor.
  constructor(props) {
    // Pass props to super.
    super(props)

    // Const assignment.
    const { _item } = this.props
    let _date,
      _endDate,
      _startDate

    // Variable assignment.
    _endDate = void 0

    // Const assignment.
    const _separator = _item && _item.separator ? _item.separator : '-'
    const _dateFormat = _item && _item.format ? _item.format : 'llll'

    // If date is defined than start parsing it.
    if (_date) {
      const dates = _date.split(` ${_separator} `)
      _startDate = moment(dates[0], _dateFormat)
      _endDate = moment(dates[1], _dateFormat)
    }

    // State assignment.
    this.state = {
      focusedInput: null,
      startDate: _startDate,
      endDate: _endDate,
      dateFormat: _dateFormat
    }

    // Handlers for handling and selecting dates.
    this.onDateChangeFunc = this.onDateChangeFunc.bind(this)
    this.onFocusChangeFunc = this.onFocusChangeFunc.bind(this)
  }

  // Handlers for handling dates.
  onDateChangeFunc = ({ startDate, endDate }) => this.setState({ startDate, endDate });

  onFocusChangeFunc = focusedInput => this.setState({ focusedInput });


  // Render object.
  render() {
    // Const assignment.
    const { focusedInput, startDate, endDate } = this.state
    const {
      className,
      labelText,
      labelPosition,
      item,
      startDateId,
      endDateId,
      startDatePlaceholderText,
      endDatePlaceholderText,
      disabled,
      showClearDates,
      isRTL,
      orientation,
      anchorDirection,
      withPortal,
      withFullScreenPortal,
      ...props
    } = this.props

    // Add all default class to an array
    const _addAllClasses = ['reusecore__DatePicker']

    // Add label position class
    if (labelPosition) _addAllClasses.push(`label_${labelPosition}`)

    // Label field and position control
    const _position = labelPosition || 'right'
    const _LabelField = labelText && (
      <span className='reusecore__field-label'>{labelText}</span>
    )

    // Checking if any classname is passed or not.
    if (className) _addAllClasses.push(className)

    // DatePicker Props List
    const datePickerPropsOptions = {
      startDateId: startDateId ? startDateId : 'start_unique_id',
      endDateId: endDateId ? endDateId : 'end_date_unique_id',
      startDate,
      endDate,
      focusedInput,
      startDatePlaceholderText,
      endDatePlaceholderText,
      disabled,
      isRTL,
      showClearDates,
      orientation,
      anchorDirection,
      withPortal,
      withFullScreenPortal,
      onFocusChange: this.onFocusChangeFunc,
      onDatesChange: this.onDateChangeFunc,
      ...props
    }

    // Return Component.
    return (
      <Fragment>
        <DateRangePickerStyle className={_addAllClasses.join(' ')}>
          <label>
            {'left' === _position || 'right' === _position || 'top' === _position ? _LabelField : null}
            <DateRangePicker {...datePickerPropsOptions} />
            <div>
              <div />
            </div>
            {'bottom' === _position && _LabelField}
          </label>
        </DateRangePickerStyle>
      </Fragment>
    )
  }
}


/*
 * PROPTYPES
 */
DateRangePickerBox.propTypes = {
  labelText: PropTypes.string,
  labelPosition: PropTypes.oneOf(['top', 'bottom', 'right', 'left']),
  startDateId: PropTypes.string.isRequired,
  endDateId: PropTypes.string.isRequired,
  startDatePlaceholderText: PropTypes.string,
  endDatePlaceholderText: PropTypes.string,
  disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(['START_DATE', 'END_DATE'])]),
  showClearDate: PropTypes.bool,
  isRTL: PropTypes.bool,
  orientation: PropTypes.oneOf(['horizontal', 'vertical']),
  anchorDirection: PropTypes.oneOf(['left', 'right']),
  withPortal: PropTypes.bool,
  withFullScreenPortal: PropTypes.bool
}
DateRangePickerBox.defaultProps = {
  labelText: 'ReuseCore DateRangePickerBox',
  labelPosition: 'top'
}


/*
 * EXPORTS
 */
export default DateRangePickerBox
