/*
 * IMPORTS
 */
import 'react-dates/lib/css/_datepicker.css' // NPM: React datePicker css.
import PropTypes from 'prop-types' // NPM: PropTypes library.
import { SingleDatePicker } from 'react-dates' // NPM: React.js library.
import { Component, Fragment } from 'react' // NPM: React.js Library.


/*
 * STYLES
 */
import DatePickerStyle from './index.style'


/*
 * OBJECTS
 */
class DatePicker extends Component {
  // Constructor.
  constructor(props) {
    // Pass props to super.
    super(props)

    // Const assignment.
    const date = null

    // State assignment.
    this.state = {
      focused: false,
      date,
      dateFormat: 'l'
    }
    this.onDateChangeFunc = this.onDateChangeFunc.bind(this)
    this.onFocusChangeFunc = this.onFocusChangeFunc.bind(this)
  }

  // DateChange Objects.
  onDateChangeFunc = date => this.setState({ date });

  onFocusChangeFunc = ({ focused }) => this.setState({ focused });


  // Render object
  render() {
    // Const assignment.
    const { focused, date } = this.state
    const {
      className,
      labelText,
      labelPosition,
      item,
      placeholder,
      disabled,
      showClearDate,
      isRTL,
      orientation,
      anchorDirection,
      withPortal,
      withFullScreenPortal,
      ...props
    } = this.props

    // Default class of datepicker.
    const _addAllClasses = ['reusecore__DatePicker']

    // Add label position class
    if (labelPosition) _addAllClasses.push(`label_${labelPosition}`)

    // Label position and text control.
    const _position = labelPosition || 'right'
    const _LabelField = labelText && (
      <span className='reusecore__field-label'>{labelText}</span>
    )

    // ClassName prop checking if any passed.
    if (className) _addAllClasses.push(className)

    // DatePicker Props List and options.
    const datePickerPropsOptions = {
      id: item && item.id ? item.id : 'unique_id',
      date,
      focused,
      placeholder,
      disabled,
      isRTL,
      showClearDate,
      orientation,
      anchorDirection,
      withPortal,
      withFullScreenPortal,
      onFocusChange: this.onFocusChangeFunc,
      onDateChange: this.onDateChangeFunc,
      ...props
    }

    // Return component.
    return (
      <Fragment>
        <DatePickerStyle className={_addAllClasses.join(' ')}>
          <label>
            {'left' === _position || 'right' === _position || 'top' === _position ? _LabelField : null}
            <SingleDatePicker {...datePickerPropsOptions} />
            <div>
              <div />
            </div>
            {'bottom' === _position && _LabelField}
          </label>
        </DatePickerStyle>
      </Fragment>
    )
  }
}


/*
 * PROPTYPES
 */
DatePicker.propTypes = {
  /** LabelText of the date-picker field */
  labelText: PropTypes.string,
  /** LabelText of the date-picker field */
  labelPosition: PropTypes.oneOf(['top', 'bottom', 'right', 'left']),
  /** Placeholder of the date-picker field */
  placeholder: PropTypes.string,
  /** Disabled of the date-picker field */
  disabled: PropTypes.bool,
  /** ShowClearDate of the date-picker field */
  showClearDate: PropTypes.bool,
  /** IsRTL of the date-picker field */
  isRTL: PropTypes.bool,
  /** Orientation of the date-picker field */
  orientation: PropTypes.oneOf(['horizontal', 'vertical']),
  /** AnchorDirection of the date-picker field */
  anchorDirection: PropTypes.oneOf(['left', 'right']),
  /** WithPortal of the date-picker field */
  withPortal: PropTypes.bool,
  /** WithFullScreenPortal of the date-picker field */
  withFullScreenPortal: PropTypes.bool
}
DatePicker.defaultProps = {
  labelText: 'ReuseCore DatePicker',
  labelPosition: 'top'
}


/*
 * EXPORTS
 */
export default DatePicker
