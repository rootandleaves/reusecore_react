/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: Styled components Library.
import ThemedStyleSheet from 'react-with-styles/lib/ThemedStyleSheet' // NPM: For registering your own Aphrodite interface.
import aphroditeInterface from 'react-with-styles-interface-aphrodite' // NPM: AphroditeInterface to style react components.
import DefaultTheme from 'react-dates/lib/theme/DefaultTheme' // NPM: React-dates Theme default styles.


/*
 * GLOBALS
 */
ThemedStyleSheet.registerInterface(aphroditeInterface)
ThemedStyleSheet.registerTheme(DefaultTheme)


/*
 * OBJECTS
 */
const DatePickerStyle = styled.div``
DatePickerStyle.displayName = 'DatePickerStyle'


/*
 * PROPTYPES
 */
DatePickerStyle.propTypes = {}
DatePickerStyle.defaultProps = {}


/*
 * EXPORTS
 */
export default DatePickerStyle
