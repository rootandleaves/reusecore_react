/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props handler.
import _ from 'underscore' // NPM: utility module.
import { useDebouncedCallback } from 'use-debounce' // NPM: debounce library.
import { useState, useEffect } from 'react' // NPM: React.js Library.


/*
 * STYLES
 */
import InputField, { EyeButton, UploadButton } from './index.style'


/*
 * GLOBALS
 */
const _lastValue = new Map()


/*
 * OBJECTS
 */
const Input = ({
  label,
  value,
  delay,
  onBlur,
  onFocus,
  onChange,
  inputType,
  isMaterial,
  icon,
  iconPosition,
  passwordShowHide,
  className,
  showFileUploadIcon,
  name,
  passValue,
  ...props
}) => {
  // Hook assignment.
  const [state, setState] = useState({
    'toggle': false,
    'focus': false,
    'value': value ?? _lastValue.get(name) ?? ''
  })
  const _DebouncedOnChange = useDebouncedCallback(() => onChange(state.value), delay)

  // Toggle function
  const HandleToggle = () => {
    setState({
      ...state,
      'toggle': !state.toggle
    })
  }

  // Add focus class
  const HandleOnFocus = event => {
    setState({
      ...state,
      'focus': true
    })
    onFocus(event)
  }

  // Remove focus class
  const HandleOnBlur = event => {
    setState({
      ...state,
      'focus': false
    })
    onBlur(event)
  }

  // Handle input value
  const HandleOnChange = event => {
    setState({
      ...state,
      'value': event.target.value
    })
    _lastValue.set(name, event.target.value)

    // Cancel all previous debounce.
    _DebouncedOnChange.cancel()

    // Delay input change.
    _.isNumber(delay) ? _DebouncedOnChange() : onChange(event.target.files ?? event.target.value)
  }

  // Get input focus class
  const GetInputFocusClass = () => {
    // If focus is passed or value.
    if (true === state.focus || '' !== state.value) return 'is-focus'

    // Return empty screen.
    return ''
  }

  // Init variable
  let _htmlFor, _inputElement

  // Add all class to an array
  const _addAllClasses = ['reusecore__input']

  // Add is-material class
  if (isMaterial) {
    _addAllClasses.push('is-material')
  }

  // Add icon position class if input element has icon
  if (icon && iconPosition) {
    _addAllClasses.push(`icon-${iconPosition}`)
  }

  // Add new class
  if (className) {
    _addAllClasses.push(className)
  }

  // If label is not empty
  if (label) {
    _htmlFor = label.replace(/\s+/ug, '_').toLowerCase()
  }

  // Label position
  const LabelPosition = true === isMaterial ? 'bottom' : 'top'

  // Label field
  const LabelField = label && <label htmlFor={_htmlFor}>{label}</label>

  // Event listener.
  useEffect(() => () => { _lastValue.delete(name) }, [])

  // Input type check
  switch (inputType) {
  case 'textarea':
    _inputElement = (
      <textarea
        {...props}
        id={_htmlFor}
        name={_htmlFor}
        value={state.value}
        onChange={HandleOnChange}
        onBlur={HandleOnBlur}
        onFocus={HandleOnFocus}
      />
    )

    // Break text area;
    break
  case 'password':
    _inputElement = (
      <div className='field-wrapper'>
        <input
          {...props}
          id={_htmlFor}
          name={_htmlFor}
          type={state.toggle ? 'text' : 'password'}
          value={state.value}
          onChange={HandleOnChange}
          onBlur={HandleOnBlur}
          onFocus={HandleOnFocus}
        />
        {passwordShowHide && (
          <EyeButton
            onClick={HandleToggle}
            className={state.toggle ? 'eye' : 'eye-closed'}
          >
            <span />
          </EyeButton>
        )}
      </div>
    )

    // Break password field.
    break
  case 'file':
    _inputElement = (
      <div className='field-wrapper'>
        <input
          {...props}
          id={_htmlFor}
          name={_htmlFor}
          type='file'
          value={passValue ? value : state.value}
          onChange={HandleOnChange}
          onBlur={HandleOnBlur}
          onFocus={HandleOnFocus}
        />
        {showFileUploadIcon && (
          <UploadButton
            onClick={HandleToggle}
            className='file-upload'
          >
            <span />
          </UploadButton>
        )}
      </div>
    )

    // Break password field.
    break
  default:
    _inputElement = (
      <div className='field-wrapper'>
        <input
          {...props}
          id={_htmlFor}
          name={_htmlFor}
          type={inputType}
          value={passValue ? value : state.value}
          onChange={HandleOnChange}
          onBlur={HandleOnBlur}
          onFocus={HandleOnFocus}
        />
        {icon && <span className='input-icon'>{icon}</span>}
      </div>
    )
  }

  // Return input field.
  return (
    <InputField className={`${_addAllClasses.join(' ')} ${GetInputFocusClass()}`}>
      {'top' === LabelPosition && LabelField}
      {_inputElement}
      {isMaterial && <span className='primary' />}
      {'bottom' === LabelPosition && LabelField}
    </InputField>
  )
}


/*
 * PROPTYPES
 */
Input.propTypes = {
  /** ClassName of the Input component. */
  className: PropTypes.string,

  /** Set input label value. */
  label: PropTypes.string,

  /** The input value, required for a controlled component. */
  value: PropTypes.any,

  /** Make default input into material style input. */
  isMaterial: PropTypes.bool,

  /** Password show hide icon button prop [*only for password field]. */
  passwordShowHide: PropTypes.bool,

  /** Set input type of the input element. Default type is text. */
  inputType: PropTypes.oneOf([
    'text',
    'email',
    'password',
    'number',
    'textarea',
    'file',
    'date'
  ]),

  /**
   * Add icon in input field. This prop will not work with password
   * and textarea field.
   */
  icon: PropTypes.object,

  /** Set input field icon position. Default position is 'left'. */
  iconPosition: PropTypes.oneOf(['left', 'right']),

  /**
   * @ignore
   */
  onBlur: PropTypes.func,

  /**
   * @ignore
   */
  onFocus: PropTypes.func,

  /**
   * Callback fired when the value is changed.
   *
   * @param {object} event The event source of the callback.
   * You can pull out the new value by accessing `event.target.value`.
   */
  onChange: PropTypes.func
}
Input.defaultProps = {
  inputType: 'text',
  isMaterial: false,
  iconPosition: 'left',
  // eslint-disable-next-line no-empty-function
  onBlur() {},
  // eslint-disable-next-line no-empty-function
  onFocus() {},
  // eslint-disable-next-line no-empty-function
  onChange() {}
}


/*
 * EXPORTS
 */
export default Input
