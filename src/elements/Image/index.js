/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Proptypes handler.
import styled from 'styled-components' // NPM: React.js styled-components Library.
import __Image from 'next/image' // NPM: next.js image support.


/*
 * SIBLINGS
 */
import { base, themed } from '../base'


/*
 * OBJECTS
 */
const ImageWrapper = styled(__Image)(
  {
    display: 'block',
    maxWidth: '100%',
    height: 'auto'
  },
  base,
  themed('Image')
)
const Image = ({ src, ...props }) => <ImageWrapper src={src} {...props} />


/*
 * PROPTYPES
 */
Image.propTypes = {
  /** Image src location */
  src: PropTypes.string.isRequired
}
Image.defaultProps = {
  m: 0
}


/*
 * EXPORTS
 */
export default Image
