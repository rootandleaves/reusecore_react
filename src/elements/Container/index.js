/*
 * STYLES
 */
import ContainerWrapper from './index.style'


/*
 * OBJECTS
 */
const Container = ({
  children,
  className,
  fullWidth,
  noGutter,
  mobileGutter,
  width
}) => {
  // Add all class to an array
  const _addAllClasses = ['container']

  // ClassName prop checking
  if (className) _addAllClasses.push(className)

  // Return component.
  return (
    <ContainerWrapper
      className={_addAllClasses.join(' ')}
      fullWidth={fullWidth}
      noGutter={noGutter}
      width={width}
      mobileGutter={mobileGutter}
    >
      {children}
    </ContainerWrapper>
  )
}


/*
 * EXPORTS
 */
export default Container
