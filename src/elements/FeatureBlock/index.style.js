/*
 * IMPORTS
 */
import styled from 'styled-components' // NPM: styled component library.
import {
  alignItems,
  borderColor,
  borderRadius,
  borders,
  boxShadow,
  color,
  display,
  flexDirection,
  flexWrap,
  fontSize,
  height,
  justifyContent,
  overflow,
  position,
  space,
  textAlign,
  width
} from 'styled-system' // NPM: styled system library.


/*
 * OBJECTS
 */
const FeatureBlockWrapper = styled.div`
  &.icon_left {
    display: flex;
    .icon__wrapper {
      flex-shrink: 0;
    }
  }
  &.icon_right {
    display: flex;
    flex-direction: row-reverse;
    .content__wrapper {
      text-align: right;
    }
    .icon__wrapper {
      flex-shrink: 0;
    }
  }

  /* styled system prop support */
  ${display}
  ${width}
  ${height}
  ${flexWrap}
  ${flexDirection}
  ${alignItems}
  ${justifyContent}
  ${position}
  ${color}
  ${space}
  ${borders}
  ${borderColor}
  ${boxShadow}
  ${borderRadius}
  ${overflow}
`
const IconWrapper = styled.div`
  ${display}
  ${width}
  ${height}
  ${alignItems}
  ${justifyContent}
  ${position}
  ${color}
  ${space}
  ${borders}
  ${borderColor}
  ${boxShadow}
  ${borderRadius}
  ${overflow}
  ${fontSize}
`
const ContentWrapper = styled.div`
  ${width}
  ${space}
  ${textAlign}
`
const ButtonWrapper = styled.div`
  ${display}
  ${space}
  ${alignItems}
  ${flexDirection}
  ${justifyContent}
`


/*
 * EXPORTS
 */
export { IconWrapper, ContentWrapper, ButtonWrapper }
export default FeatureBlockWrapper
