/*
 * IMPORTS
 */
import PropTypes from 'prop-types' // NPM: Props type library.
import { Fragment } from 'react' // NPM: React.js library.


/*
 * STYLES
 */
import FeatureBlockWrapper, {
  ButtonWrapper,
  ContentWrapper,
  IconWrapper
} from './index.style'


/*
 * OBJECTS
 */
const FeatureBlock = ({
  className,
  icon,
  title,
  button,
  description,
  iconPosition,
  additionalContent,
  wrapperStyle,
  iconStyle,
  contentStyle,
  btnWrapperStyle,
  ...props
}) => {
  // Add all class to an array
  const addAllClasses = ['feature__block']

  // Add icon position class
  if (iconPosition) {
    addAllClasses.push(`icon_${iconPosition}`)
  }

  // ClassName prop checking
  if (className) {
    addAllClasses.push(className)
  }

  // Check icon value and add
  const Icon = icon && (
    <IconWrapper className='icon__wrapper' {...iconStyle}>
      {icon}
    </IconWrapper>
  )

  // Return component.
  return (
    <FeatureBlockWrapper
      className={addAllClasses.join(' ')}
      {...wrapperStyle}
      {...props}
    >
      {Icon}
      {title || description || button ? (
        <Fragment>
          <ContentWrapper className='content__wrapper' {...contentStyle}>
            {title}
            {description}
            {button && (
              <ButtonWrapper className='button__wrapper' {...btnWrapperStyle}>
                {button}
              </ButtonWrapper>
            )}
          </ContentWrapper>
          {additionalContent}
        </Fragment>
      ) : (
        ''
      )}
    </FeatureBlockWrapper>
  )
}


/*
 * PROPTYPES
 */
FeatureBlock.propTypes = {
  /** ClassName of the FeatureBlock */
  className: PropTypes.string,

  /** Title prop contain a react component. You can use our Heading component from reusecore */
  title: PropTypes.element,

  /** Description prop contain a react component. You can use our Text component from reusecore */
  description: PropTypes.element,

  /** Button prop contain a react component. You can use our Button component from reusecore */
  button: PropTypes.element,

  /** Set icon position of the FeatureBlock */
  iconPosition: PropTypes.oneOf(['top', 'left', 'right']),

  /**
   * WrapperStyle prop contain these style system props:  display, flexWrap, width, height, alignItems,
   * justifyContent, position, overflow, space, color, borders, borderColor, boxShadow and borderRadius.
   */
  wrapperStyle: PropTypes.object,

  /**
   * IconStyle prop contain these style system props: display, width, height, alignItems, justifyContent,
   * position, space, fontSize, color, borders, overflow, borderColor, boxShadow and borderRadius.
   */
  iconStyle: PropTypes.object,

  /** ContentStyle prop contain these style system props: width, textAlign and space. */
  contentStyle: PropTypes.object,

  /**
   * BtnWrapperStyle prop contain these style system props: display, space, alignItems,
   * flexDirection and justifyContent.
   */
  btnWrapperStyle: PropTypes.object
}
FeatureBlock.defaultProps = {
  iconPosition: 'top'
}


/*
 * EXPORTS
 */
export default FeatureBlock
