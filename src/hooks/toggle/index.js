/*
 * IMPORTS
 */
import { useCallback, useState } from 'react' // NPM: React.js Library


/*
 * EXPORTS
 */
export default initialValue => {
  // Const assignment.
  const [value, setValue] = useState(initialValue)
  const toggler = useCallback(() => setValue(value => !value))

  // Return value and toggle.
  return [value, toggler]
}
