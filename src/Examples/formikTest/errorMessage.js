import React, { Fragment } from 'react' // NPM: React.js Library
import Alert from '../../elements/Alert' // Alert element reusecore

const ErrorMessageComponent = props => {
  const { _error } = props

  return (
    <Fragment>
      <Alert className='error' colors='error'>
        {_error}
      </Alert>
    </Fragment>
  )
}

export default ErrorMessageComponent // Export
