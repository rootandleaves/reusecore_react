import React, { Fragment } from 'react' // NPM: React.js Library
import { Field, Formik } from 'formik' // NPM: React.js formik Library
import ErrorMessageComponent from './errorMessage' // ErrorMessage.js Import
import {
  DisplayFormikState,
  onSubmitFunc,
  validateFunc
} from './helperFunctions' // Helperfunctions file import
/*
 *Reusecore Elements
 */
import Button from '../../elements/Button'
import RadioBox from '../../elements/Radio'
import Select from '../../elements/Select'
/*
 *Data
 */
import { data, initialValues } from './data'

const RadioButton = ({
  field: { name, value, onChange, onBlur, setFieldValue },
  id,
  label,
  className,
  ...props
}) => (
  <Fragment>
    <RadioBox
      name={name}
      id={id}
      value={id} // Could be something else for output?
      checked={id === value}
      labelText={label}
      onChange={onChange}
      onBlur={onBlur}
      className='radio-button'
      {...props}
    />
  </Fragment>
)

const RadioGroupWithFormik = ({
  value,
  error,
  touched,
  id,
  label,
  children
}) => (
  <div>
    <fieldset>
      <legend>{label}</legend>
      {children}
      {touched && error && <ErrorMessageComponent error={error} />}
    </fieldset>
  </div>
)

const FormikTest = () =>
  // Render component
  (
    <Fragment>
      <Formik
        initialValues={initialValues}
        validate={validateFunc}
        onSubmit={onSubmitFunc}
      >
        {props => {
          const {
            values,
            errors,
            touched,
            dirty,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            handleReset,
            setFieldValue,
            setFieldTouched
          } = props

          return (
            <form onSubmit={handleSubmit}>
              <div>
                <input
                  type='email'
                  name='email'
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                />
                {errors.email && touched.email ? (
                  <ErrorMessageComponent error={errors.email} />
                ) : (
                  ''
                )}
              </div>
              <br />
              <div>
                <input
                  type='password'
                  name='password'
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                {errors.password && touched.password ? (
                  <ErrorMessageComponent error={errors.password} />
                ) : (
                  ''
                )}
              </div>
              <br />
              <RadioGroupWithFormik
                id='gender'
                label='Choose One of these please!'
                value={values.gender}
                error={errors.gender}
                touched={touched.gender}
                onChange={value => setFieldValue('gender', value)}
                onBlur={() => setFieldTouched('gender', true)}
              >
                {data.gender ? data.gender.map((single, index) => (
                  <Field
                    key={index}
                    component={RadioButton}
                    name='gender'
                    id={single.value}
                    value={single.value}
                    label={single.label}
                  />
                )) : ''}
                {errors.gender && touched.gender ? (
                  <ErrorMessageComponent error={errors.gender} />
                ) : (
                  ''
                )}
              </RadioGroupWithFormik>
              <br />
              <div>
                <Select
                  id='profession'
                  type='select'
                  options={data.profession}
                  value={values.profession}
                  error={errors.profession}
                  touched={touched.profession}
                  onChange={value => setFieldValue('profession', value)}
                  onBlur={() => setFieldTouched('profession', true)}
                />
                {errors.profession && touched.profession ? (
                  <ErrorMessageComponent error={errors.profession} />
                ) : (
                  ''
                )}
              </div>
              <br />
              <Fragment>
                <Button
                  type='submit'
                  disabled={isSubmitting}
                  title='Submit Form'
                  colors='primary'
                  variant='outlined'
                />
              </Fragment>
              <br />
              <Fragment>
                <Button
                  type='button'
                  className='outline'
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                  title='Reset'
                  colors='primaryWithBg'
                  varient='textButton'
                />
              </Fragment>
              <DisplayFormikState {...props} />
            </form>
          )
        }}
      </Formik>
    </Fragment>
  )


export default FormikTest // Exports
