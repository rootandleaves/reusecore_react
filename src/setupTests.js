/*
 * IMPORTS
 */
import { configure } from 'enzyme' // NPM: enzyme library.
import Adapter from 'enzyme-adapter-react-16' // NPM: enzyme react.js Support

/*
 * CONFIG
 */
configure({ adapter: new Adapter() })

/*
 * GLOBALS
 */
const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn()
}

// Save to global storage.
global.localStorage = localStorageMock
