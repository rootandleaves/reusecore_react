/*
 * EXPORTS
 */
export default {
  modifyBundlerConfig(config) {
    // Push loaders to config.
    config.module.rules.push({
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    })

    // Return config.
    return config
  }
}
